package fr.azerks.render;

import org.lwjgl.system.MemoryStack;

import java.nio.FloatBuffer;

import static org.lwjgl.opengl.GL30C.*;

public class Mesh {

    private int vao = glGenVertexArrays();
    private int vbo = glGenBuffers();

    private Shader shader = Shader.getInstance();

    public Mesh() {
        glBindVertexArray(vao);
        glBindBuffer(GL_ARRAY_BUFFER, vbo);


        try (MemoryStack stack = MemoryStack.stackPush()) {
            FloatBuffer vertices = stack.mallocFloat(6 * 6);
            vertices.put(-0.5f).put(0.5f).put(0.0f).put(1.0f).put(1.f).put(1.0f);
            vertices.put(-0.5f).put(-0.5f).put(0.0f).put(1.0f).put(1.f).put(1.f);
            vertices.put(0.5f).put(-0.5f).put(0.0f).put(1.0f).put(1.f).put(1.f);

            vertices.put(-0.5f).put(0.5f).put(0.0f).put(1.f).put(1.f).put(1.f);
            vertices.put(0.5f).put(0.5f).put(0.0f).put(1.f).put(1.f).put(1.f);
            vertices.put(0.5f).put(-0.5f).put(0.0f).put(1.f).put(1.f).put(1.f);

            vertices.flip();

            int floatSize = 4;

            glBufferData(GL_ARRAY_BUFFER, vertices, GL_STATIC_DRAW);

            int positionIndex = glGetAttribLocation(shader.programId, "position");
            glEnableVertexAttribArray(positionIndex);
            glVertexAttribPointer(positionIndex, 3, GL_FLOAT, false, floatSize * 6, 0);

            int colorIndex = glGetAttribLocation(shader.programId, "color");
            glEnableVertexAttribArray(colorIndex);
            glVertexAttribPointer(colorIndex, 3, GL_FLOAT, false, floatSize * 6, floatSize * 3);

            glBindBuffer(GL_ARRAY_BUFFER, 0);

            System.out.println("Sprite Initialised");
        }
    }

    public void render() {
        glClear(GL_COLOR_BUFFER_BIT);

        glBindVertexArray(vao);
        glEnableVertexAttribArray(0);
        glBindBuffer(GL_ARRAY_BUFFER, vbo);

        glDrawArrays(GL_TRIANGLE_STRIP, 0, 6);
        //glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
        shader.unbind();
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glDisableVertexAttribArray(0);
        glBindVertexArray(0);
    }

    public void cleanUp() {
        glDeleteVertexArrays(vao);
        glDeleteBuffers(vbo);
        glDeleteShader(shader.vertexShader);
        glDeleteShader(shader.fragmentShader);
        glDeleteProgram(shader.programId);
    }
}
