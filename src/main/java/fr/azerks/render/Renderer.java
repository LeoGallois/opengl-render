package fr.azerks.render;

import fr.azerks.settings.GameSettings;
import org.joml.Matrix4f;

public class Renderer {

    static Shader shader = Shader.getInstance();

    public static void renders(GameItem[] items) {
        shader.bind();
        shader.createUniforms("projectionMatrix", "worldMatrix");

        Transformation transformation = new Transformation();

        Matrix4f projection1 = transformation.getProjectionMatrix(GameSettings.FOV, GameSettings.width,
                GameSettings.height, GameSettings.Z_NEAR, GameSettings.Z_FAR);

        Matrix4f projection2 = new Matrix4f().perspective(GameSettings.FOV, GameSettings.aspect,
                GameSettings.Z_NEAR, GameSettings.Z_FAR);

        Matrix4f projection3 = new Matrix4f().ortho(-GameSettings.aspect, GameSettings.aspect,
                -1f, 1f, GameSettings.Z_NEAR, GameSettings.Z_FAR);

        shader.setUniform("projectionMatrix", projection3);

        for (GameItem item : items) {
            Matrix4f worldMatrix = transformation.getWorldMatrix(
                    item.getPosition(),
                    item.getRotation(),
                    item.getScale());

            shader.setUniform("worldMatrix", worldMatrix);
            item.getMesh().render();
        }
    }
}
