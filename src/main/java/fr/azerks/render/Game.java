package fr.azerks.render;

import fr.azerks.client.Inventory;
import fr.azerks.client.Player;
import fr.azerks.client.Spell;
import fr.azerks.settings.GameSettings;
import fr.azerks.settings.utils.MovementInput;
import fr.azerks.settings.utils.Timer;
import org.lwjgl.Version;
import org.lwjgl.glfw.Callbacks;
import org.lwjgl.glfw.GLFWErrorCallback;
import org.lwjgl.glfw.GLFWVidMode;
import org.lwjgl.opengl.GL;
import org.lwjgl.system.MemoryStack;

import java.nio.IntBuffer;

import static java.lang.Thread.sleep;
import static org.lwjgl.glfw.Callbacks.glfwFreeCallbacks;
import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.system.MemoryStack.stackPush;
import static org.lwjgl.system.MemoryUtil.NULL;

public class Game implements Runnable {
    static Mesh mesh;
    public static MovementInput movementInput = new MovementInput();
    public static GameSettings gameSettings = GameSettings.getInstance();
    Spell[] spells = {};
    Player player = new Player("AzerKs", 1, 0, 100, 100, new Inventory()
            , spells, new float[]{0, 0});

    private long window;
    private Timer timer = new Timer();
    private Thread game;
    private GameItem gameItem;

    public void start() {
        game = new Thread(this, "Game");
        game.start();
    }

    public void run() {
        System.out.println("LWJGL Version" + Version.getVersion());

        init();
        gameLoop();

        glfwFreeCallbacks(window);
        glfwDestroyWindow(window);

        glfwTerminate();
        glfwSetErrorCallback(null).free();
        Callbacks.glfwFreeCallbacks(window);
    }

    private void init() {
        GLFWErrorCallback.createPrint(System.err).set();

        if (!glfwInit()) {
            throw new IllegalStateException("Unable to initialize LWJGL");
        }

        glfwDefaultWindowHints();
        glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE);
        glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE);
        glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
        glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
        glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
        glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GLFW_TRUE);

        window = glfwCreateWindow(GameSettings.width, GameSettings.height, "IFK", NULL, NULL);
        if (window == NULL) throw new RuntimeException("Failed to create the GLFW window");

        glfwSetKeyCallback(window, (window, key, scancode, action, mods) -> {

            if (key == GLFW_KEY_ESCAPE && action == GLFW_RELEASE) {
                glfwSetWindowShouldClose(window, true);
            }

            movementInput.updatePlayerMovement(key, action);
        });

        try (MemoryStack stack = stackPush()) {
            IntBuffer pWidth = stack.mallocInt(1); // int*
            IntBuffer pHeight = stack.mallocInt(1); // int*

            glfwGetWindowSize(window, pWidth, pHeight);

            GLFWVidMode vidmode = glfwGetVideoMode(glfwGetPrimaryMonitor());

            glfwSetWindowPos(window,
                    (vidmode.width() - pWidth.get(0)) / 2,
                    (vidmode.height() - pHeight.get(0)) / 2
            );
        }
        timer.setLastLoopTime(glfwGetTime());

        glfwMakeContextCurrent(window);
        glfwSwapInterval(GameSettings.vsync);
        glfwShowWindow(window);
    }

    public void update() {
        glClearColor(1f, 0f, 0f, 0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        glfwPollEvents();
        player.updatePlayer();
    }

    private void gameLoop() {
        long targetTime = 1000L / GameSettings.fpsLimit;

        GL.createCapabilities();

        mesh = new Mesh();
        gameItem = new GameItem(mesh);
        System.out.println("OpenGL version " + glGetString(GL_VERSION));

        while (!glfwWindowShouldClose(window)) {

            double startTime = timer.getTime() * 1000;

            update();
            render();

            double endTime = timer.getTime() * 1000;

            // if vsync is off the game is update at 60 fps
            if (GameSettings.vsync == 0) {
                try {
                    sleep((long) (startTime + targetTime - endTime));
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
        exit();
    }

    private void render() {
        Renderer.renders(new GameItem[]{gameItem});
        glfwSwapBuffers(window);
    }

    private void exit() {
        mesh.cleanUp();
        game.interrupt();
    }
}
