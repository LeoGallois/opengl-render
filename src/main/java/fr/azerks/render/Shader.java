package fr.azerks.render;


import org.joml.Matrix4f;
import org.lwjgl.system.MemoryStack;

import java.io.BufferedReader;
import java.io.FileReader;
import java.nio.FloatBuffer;
import java.util.HashMap;
import java.util.Map;

import static org.lwjgl.opengl.GL30.*;

public class Shader {

    public static int programId = glCreateProgram();

    private static Shader instance = new Shader();
    protected static int vertexShader;
    protected static int fragmentShader;
    private static Map<String, Integer> uniforms = new HashMap<>();

    private Shader() {
        initShader();
    }

    public static Shader getInstance() {
        return instance;
    }

    private static void initShader() {
        String vertSource = loadFile("src/main/resources/shader/shader.vert");
        String fragSource = loadFile("src/main/resources/shader/shader.frag");

        vertexShader = glCreateShader(GL_VERTEX_SHADER);
        fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);

        glShaderSource(vertexShader, vertSource);
        glShaderSource(fragmentShader, fragSource);

        glCompileShader(vertexShader);
        glCompileShader(fragmentShader);

        glAttachShader(programId, vertexShader);
        glAttachShader(programId, fragmentShader);

        glLinkProgram(programId);

        glValidateProgram(programId);

        shaderCheck(programId, vertexShader, fragmentShader);

        glDeleteShader(vertexShader);
        glDeleteShader(fragmentShader);

        System.out.println("Shader Initialised");
    }

    private static void shaderCheck(int shaderProgram, int vertexShader, int fragmentShader) {
        if (glGetShaderi(fragmentShader, GL_COMPILE_STATUS) != GL_TRUE)
            throw new RuntimeException(glGetShaderInfoLog(fragmentShader));

        if (glGetShaderi(vertexShader, GL_COMPILE_STATUS) != GL_TRUE)
            throw new RuntimeException(glGetShaderInfoLog(vertexShader));

        if (glGetProgrami(shaderProgram, GL_LINK_STATUS) != GL_TRUE)
            throw new RuntimeException(glGetProgramInfoLog(shaderProgram));

        if (glGetProgrami(shaderProgram, GL_VALIDATE_STATUS) != GL_TRUE)
            throw new RuntimeException(glGetProgramInfoLog(shaderProgram));

    }

    public void bind() {
        glUseProgram(programId);
    }

    public void unbind() {
        glUseProgram(0);
    }

    public void destroy() {
        glDeleteProgram(programId);
    }

    public static void createUniform(String uniformName) {
        int uniformId = glGetUniformLocation(programId, uniformName);

        if (uniformId < 0) try {
            throw new Exception("Could't find uniform " + uniformName);
        } catch (Exception e) {
            e.printStackTrace();
        }

        uniforms.put(uniformName, uniformId);
    }

    public static void createUniforms(String... uniformNames) {
        for (String uniformName : uniformNames) {
            int uniformId = glGetUniformLocation(programId, uniformName);

            if (uniformId < 0) try {
                throw new Exception("Could't find uniform " + uniformName);
            } catch (Exception e) {
                e.printStackTrace();
            }

            uniforms.put(uniformName, uniformId);
        }
    }

    public static void setUniform(String uniformName, Matrix4f value) {
        try (MemoryStack stack = MemoryStack.stackPush()) {
            FloatBuffer fb = stack.mallocFloat(16); // Matrix Size * TypeSize
            value.get(fb);
            glUniformMatrix4fv(uniforms.get(uniformName), false, fb);
        }
    }

    private static String loadFile(String filename) {
        StringBuilder vertexCode = new StringBuilder();
        String line = null;
        try {
            BufferedReader reader = new BufferedReader(new FileReader(filename));
            while ((line = reader.readLine()) != null) {
                vertexCode.append(line);
                vertexCode.append('\n');
            }
        } catch (Exception e) {
            throw new IllegalArgumentException("unable to load shader from file [" + filename + "]", e);
        }

        return vertexCode.toString();
    }
}
