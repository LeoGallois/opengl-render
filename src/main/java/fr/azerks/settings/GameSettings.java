package fr.azerks.settings;


public class GameSettings {

    public static int width = 900;
    public static int height = 550;
    public static long fpsLimit = 60L; // 62.5
    public static int vsync = 1;

    public static final float FOV = (float) Math.toRadians(90.0f);
    public static final float Z_NEAR = -1f;
    public static final float Z_FAR = 1000f;
    public static final float aspect = width / height;

    private static GameSettings instance = new GameSettings();
    public KeyBinding keyBindForward = new KeyBinding("forward", 90);
    public KeyBinding keyBindBack = new KeyBinding("forward", 83);
    public KeyBinding keyBindRight = new KeyBinding("forward", 68);
    public KeyBinding keyBindLeft = new KeyBinding("forward", 81);

    public static GameSettings getInstance() {
        return instance;
    }
}
