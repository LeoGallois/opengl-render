package fr.azerks.settings.utils;

import fr.azerks.settings.GameSettings;

import static org.lwjgl.glfw.GLFW.GLFW_PRESS;

public class MovementInput {

    static GameSettings gameSettings = GameSettings.getInstance();

    public float moveStrafe = 0.0F;
    public float moveForward = 0.0F;

    public void updatePlayerMovement(int key, int action) {
        System.out.println(gameSettings);
        if (gameSettings.keyBindForward.getKeyCode() == key && action == GLFW_PRESS) {
            ++moveForward;
        }

        if (gameSettings.keyBindBack.getKeyCode() == key && action == GLFW_PRESS) {
            --moveForward;
        }

        if (gameSettings.keyBindRight.getKeyCode() == key && action == GLFW_PRESS) {
            ++moveStrafe;
        }

        if (gameSettings.keyBindLeft.getKeyCode() == key && action == GLFW_PRESS) {
            --moveStrafe;
        }
    }
}
