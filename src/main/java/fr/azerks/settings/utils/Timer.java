package fr.azerks.settings.utils;

import static org.lwjgl.glfw.GLFW.glfwGetTime;

public class Timer {
    public double lastLoopTime;
    int fps;
    int fpsCount;
    int ups;
    int upsCount;
    private float timeCount;

    public float getDelta() {
        double time = glfwGetTime();
        float delta = (float) (time - lastLoopTime);
        lastLoopTime = time;
        timeCount += delta;
        return delta;
    }

    public double getTime() {
        return glfwGetTime();
    }

    public void setLastLoopTime(double lastLoopTime) {
        this.lastLoopTime = lastLoopTime;
    }

    public void updateFps() {
        fpsCount++;
    }

    public void updateUps() {
        upsCount++;
    }

    public void update() {
        if (timeCount > 1f) {
            fps = fpsCount;
            fpsCount = 0;

            ups = upsCount;
            upsCount = 0;

            timeCount -= 1f;
        }
    }
}
