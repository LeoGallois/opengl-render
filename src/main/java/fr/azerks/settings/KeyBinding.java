package fr.azerks.settings;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class KeyBinding {

    private static HashMap<Integer, KeyBinding> keys = new HashMap<>();
    private static List<KeyBinding> keyBindArray = new ArrayList<>();
    private String keyDesc;
    private int keyCode;

    public KeyBinding(String desc, int keyCode) {
        this.keyDesc = desc;
        this.keyCode = keyCode;
        keyBindArray.add(this);
        keys.put(keyCode, this);
    }

    public static HashMap<Integer, KeyBinding> getKeys() {
        return keys;
    }

    public static List<KeyBinding> getKeyBindArray() {
        return keyBindArray;
    }

    public int getKeyCode() {
        return keyCode;
    }

    public String getKeyDesc() {
        return keyDesc;
    }
}
