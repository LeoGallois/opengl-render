package fr.azerks;

import fr.azerks.render.Game;

public class Main {
    public static void main(String[] args) throws InterruptedException {
        new Game().start();
    }
}
